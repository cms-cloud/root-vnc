FROM ubuntu:24.04

LABEL maintainer.name="CMS Data Preservation and Open Access Group"
LABEL maintainer.email="cms-dpoa-coordinators@cern.ch"

ENV LANG=C.UTF-8

ARG PLATFORM="linux/amd64"
ARG ROOT_VERSION_SCM=6.30.08
ARG TARGET_BRANCH=v6-30-08
ARG GIT_PROJECT_URL=https://github.com/root-project/root


WORKDIR /usr/local

SHELL [ "/bin/bash", "-c" ]

COPY packages.txt packages.txt
COPY requirements.txt requirements.txt
COPY cython_constraint.txt cython_constraint.txt

# Set PATH to pickup virtualenv by default
ENV PATH=/usr/local/venv/bin:"${PATH}"
RUN apt-get -qq -y update && \
    DEBIAN_FRONTEND=noninteractive apt-get -qq -y install --no-install-recommends $(cat packages.txt) && \
    yes | unminimize && \
    apt-get -y autoclean && \
    apt-get -y autoremove && \
    rm -rf /var/lib/apt/lists/* && \
    python -m venv /usr/local/venv && \
    . /usr/local/venv/bin/activate && \
    python -m pip --no-cache-dir install --upgrade pip setuptools wheel && \
    PIP_CONSTRAINT=cython_constraint.txt python -m pip --no-cache-dir install --requirement requirements.txt && \
    python -m pip list && \
    ln --symbolic --force /bin/bash /bin/sh && \
    ln -sf /usr/share/zoneinfo/UTC /etc/localtime

# Add XRootD under /usr/local
ARG XROOTD_TAG=v5.6.9
WORKDIR /code
RUN git clone --depth 1 https://github.com/xrootd/xrootd.git \
        --branch "${XROOTD_TAG}" \
        --single-branch \
        xrootd \
    && cmake \
        -DCMAKE_INSTALL_PREFIX=/usr/local/venv \
        -DPIP_VERBOSE=ON \
        -S xrootd \
        -B build \
    && cmake build -LH \
    && cmake \
        --build build \
        --clean-first \
        --parallel $(($(nproc) - 1)) \
    && cmake --build build --target install \
    && cd / \
    && rm -rf /code

WORKDIR /code
# c.f. https://root.cern.ch/building-root#options
RUN git clone --depth 1 "${GIT_PROJECT_URL}" \
        --branch "${TARGET_BRANCH}" \
        --single-branch \
        root_src \
    && cmake \
        -Dall=OFF \
        -Dsoversion=ON \
        -DCMAKE_CXX_STANDARD=17 \
        -Droot7=ON \
        -Dfortran=OFF \
        -Droofit=ON \
        -Droofit_multiprocess=ON \
        -Droostats=ON \
        -Dhistfactory=ON \
        -Dminuit2=ON \
        -DCMAKE_CXX_FLAGS=-D__ROOFIT_NOBANNER \
        -Dxrootd=ON \
        -Dbuiltin_xrootd=OFF \
        -DXROOTD_ROOT_DIR=/usr/local/venv \
        -Dvdt=ON \
        -Dbuiltin_vdt=ON \
        -Dpyroot=ON \
        -DPYTHON_EXECUTABLE=$(which python3) \
        -DCMAKE_INSTALL_PREFIX=/code/root_install \
        -B build \
        root_src \
    && cmake \
        --build build \
        --target install \
        --parallel $(($(nproc) - 1)) \
    && cd / \
    && cp -r /code/root_install/* /usr/local/venv \
    && rm -rf /code

RUN echo /usr/local/venv/lib >> /etc/ld.so.conf \
    && ldconfig

RUN git clone --depth 1 --single-branch https://github.com/novnc/noVNC.git /usr/local/novnc

RUN groupadd -g 1001 cmsusr && adduser --uid 1001 --gid 1001 cmsusr --disabled-password && \
    usermod -aG sudo cmsusr && \
    chown -R cmsusr:cmsusr /usr/local/venv

RUN echo "source /usr/local/vnc_utils.sh" >> /home/cmsusr/.bashrc && \
    echo "source /usr/local/vnc_utils.sh" >> /home/cmsusr/.zshrc && \
    printf '\nexport PATH=/usr/local/venv/bin:"${PATH}"\n' >> /home/cmsusr/.bashrc

COPY  vnc/vnc_utils.sh /usr/local/vnc_utils.sh

# Use C.UTF-8 locale to avoid issues with ASCII encoding
ENV LC_ALL=C.UTF-8
ENV LANG=C.UTF-8
ENV ROOTSYS=/usr/local/venv
ENV PYTHONPATH=$ROOTSYS/lib:$PYTHONPATH
ENV LD_LIBRARY_PATH=$ROOTSYS/lib:$LD_LIBRARY_PATH
ENV PATH $ROOTSYS/bin:$PATH
ENV CLING_STANDARD_PCH none

RUN mkdir /code && \
    chown -R cmsusr:cmsusr /code && \
    chmod 777 /code

WORKDIR /code
USER cmsusr
ENV USER cmsusr
ENV HOME /home/cmsusr
ENV GEOMETRY 1920x1080

RUN mkdir -p ${HOME}/.vnc
ADD --chown=cmsusr:cmsusr vnc/passwd ${HOME}/.vnc/passwd
ADD --chown=cmsusr:cmsusr .rootrc ${HOME}/.rootrc

ENTRYPOINT ["/bin/bash", "-l", "-c"]
CMD ["/bin/bash"]
