# ROOT VNC

ROOT images with VNC server

[![pipeline status](https://gitlab.cern.ch/cms-cloud/root-vnc/badges/master/pipeline.svg)](https://gitlab.cern.ch/cms-cloud/root-vnc/commits/master)

The Docker images contain ROOT 6 coupled with Python 3 and a VNC server for access to the graphical user interface.

## Usage

```shell
docker run --rm -it -P -p 5901:5901 -p 6080:6080 gitlab-registry.cern.ch/cms-cloud/root-vnc:latest
```

If you would like to mount directories from your local file system into the container, please use `/code` as target directory in the container (and in particular do not mount to `/home/cmsusr` since this will break the VNC setup).

To start the VNC server, run in the container:

```shell
start_vnc
```

The VNC server will be available on `localhost:5901` and also be accessible via a webbrowser at [http://localhost:6080]([http://localhost:6080]). The connection password is `cms.cern`.
